<?php

/**
 * Implementation of hook_perm().
 */
function body_bg_perm() {
  return array('add custom backgrounds');
}

/**
 * Implementation of hook_menu().
 */
function body_bg_menu() {
  $items = array();
  $items['admin/settings/body-backgrounds'] = array(
    'title' => 'Custom Backgrounds',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('body_bg_list_form'),
    'access arguments' => array('add custom backgrounds'),
  );
  $items['admin/settings/body-backgrounds/%'] = array(
    'title' => 'Custom Background',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('body_bg_action_form', 3, 4),
    'access arguments' => array('add custom backgrounds'),
  );  

  return $items;
}

/**
 * Implementation of hook_init().
 */
function body_bg_init() {
  // get all paths
  $bg_list = array();
  $result = db_query("SELECT * FROM {body_bg} ORDER BY weight");
  if ($result) {
    while($row = db_fetch_array($result)) {
      $bg_list[] = $row;
    }
  }
  $page_match = FALSE;
  $path = drupal_get_path_alias($_GET['q']);
  foreach ($bg_list as $bg) {
    $css_path = body_bg_css_path($bg['bid']);
    if (!file_exists($css_path)) {
      // create css file
      create_css_file($bg);
    }
    // Compare with the internal and path alias (if any).
    $page_match = drupal_match_path($path, $bg['paths']);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $bg['paths']);
    }
    if ($page_match) break;
  }
  if ($page_match) {
    drupal_add_css(body_bg_css_path($bg['bid']), 'module', 'all', FALSE);
  }
}

/**
 * Helper function to get CSS file path for a bid
 */
function body_bg_css_path($bid) {
  if (!empty($bid)) {
    $directory = file_directory_path();
    $dir = $directory . '/body-backgrounds/';
    if (!file_exists($dir)) {
      mkdir($dir);
    }
    return $dir. $bid .'.css';
  }
  return NULL;
}

function create_css_file($bg) {
  $bg = (object) $bg;
  // create css file
  $file = db_fetch_object(db_query("SELECT * FROM {files} WHERE fid = %d", $bg->fid));
  $css_file = body_bg_css_path($bg->bid);
  file_create_path($css_file);
  $css = <<<EOD
body.bg {
EOD;
  if ($file) {
    $css .= <<<EOD
  background-image: url("/{$file->filepath}");
EOD;
  }
  $css .= <<<EOD
  background-color: {$bg->bgcolor};
}
EOD;
  $putted = file_put_contents($css_file, $css);
}

function body_bg_list_form($form_state) {
  $bg_list = array();
  $result = db_query("SELECT * FROM {body_bg} ORDER BY weight");
  if ($result) {
    while($row = db_fetch_array($result)) {
      $bg_list[] = $row;
    }
  }  
  $form['addbg'] = array(
    '#prefix' => '<div>',
    '#value' => l(t('add new background'), 'admin/settings/body-backgrounds/add'),
    '#suffix' => '</div>',
  );
  // add bg names
  $form['bg_names'] = array(
    '#tree' => TRUE,
    '#theme' => 'body_bg_path_rules',
  );
  foreach ($bg_list as $bg) {
    $bid = $bg['bid'];
    $result = db_query("SELECT * FROM {files} WHERE fid = %d", $bg['fid']);
    $file = db_fetch_object($result);
    //create a partial table row containing the data from the table
    $data = array(
      $bg['name'],
      l($file->filename, $file->filepath, array('attributes' => array('target' => '_blank'))),
      l(t('edit'), 'admin/settings/body-backgrounds/edit/'.$bid),
      l(t('delete'), 'admin/settings/body-backgrounds/delete/'.$bid),
    );
    //add our static "row" data into a form value
    $form['bg_names']['rows'][$bid]['data'] = array(
      '#type' => 'value',
      '#value' => $data
    );
    //now create the weight form element.
    //NOTE how we add the id into the element key
    $form['bg_names']['rows'][$bid]['weight-'.$bid]=array(
      '#type' => 'weight',
      '#default_value' => $row->weight,
      //add a specific class in here - we need this later
      '#attributes' => array('class' => 'weight'),
    );
  }
  // add the submit button
  $form['submit']=array(
    '#type'=>'submit',
    '#value'=>t('Save changes'),
  );
  $form['#submit'][] = 'body_bg_path_rules_submit';

  return $form;
}

function body_bg_action_form($form_state, $action, $bid = NULL) {
  if ($action == 'edit' || $action == 'delete') {
    $result = db_query("SELECT * FROM {body_bg} WHERE bid = '%d'", $bid);
    if ($result) {
      $bg = db_fetch_object($result);
    }
  }
  if ($action == 'delete') {
    file_delete(file_create_path(body_bg_css_path($bg->bid)));
    db_query("DELETE FROM {files} WHERE fid = '%d'", $bg->fid);
    db_query("DELETE FROM {body_bg} WHERE bid = '%d'", $bg->bid);
    // clear css cache so that new content (as above) gets loaded in the
    // aggregated CSS (when CC optimixation is on)
    drupal_clear_css_cache();
    drupal_set_message("Successfully deleted custom background '".$bg->name."'");
    drupal_goto("admin/settings/body-backgrounds");
    return;
  }
  if ($action == 'add' || $action == 'edit') {
    $form['#attributes'] = array('enctype' => "multipart/form-data");
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Background Name'),
      '#default_value' => $bg->name,
    );
    if ($bg && $bg->fid) {
      $result = db_query("SELECT * FROM {files} WHERE fid = %d", $bg->fid);
      $file = db_fetch_object($result);
    }

    $form['upload'] = array(
      '#type' => 'file',
      '#title' => t('Background Image'),
      '#suffix' => $file ? "<p>" . t('Current image') . ': '. l($file->filename, $file->filepath) . "</p>" : '',
    );
    $form['bgcolor'] = array(
      '#type' => 'textfield',
      '#size' => 7,
      '#title' => t('Background Color'),
      '#default_value' => $bg ? $bg->bgcolor : '#000000',
    );
    $description = t("Enter a Drupal path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));   
    $form['paths'] = array(
      '#type' => 'textarea',
      '#title' => t('Paths'),
      '#default_value' => $bg->paths,
      '#description' => $description,
    );     
  }
  if ($action == 'edit' && $bg) {
    $form['bid'] = array(
      '#type' => 'hidden',
      '#value' => $bid,
    );
  }
  $form['submit'] = array(
    '#prefix' => '<div style="float:left">',
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#suffix' => '</div>',
  );  
  global $base_url;
  $url = $base_url.'/admin/settings/body-backgrounds';
  $html = '<input type="button" value="Back" onclick="javascript: window.location.href=\''.$url.'\'"/>';
  $form['back'] = array(
    '#prefix' => '<div style="float:left;margin-left:20px">',
    '#value' => $html,
    '#suffix' => '</div>',
  );
  $form['#redirect'] = 'admin/settings/body-backgrounds';
  return $form;
}

function body_bg_action_form_validate($form, &$form_state) {
  $fieldName = 'upload';
  if (isset($_FILES['files']) && is_uploaded_file($_FILES['files']['tmp_name'][$fieldName])) {
    // attempt to save the uploaded file
    $validators = array(
      'file_validate_is_image' => array(),
    );
    $file = file_save_upload($fieldName, $validators, file_directory_path());

    // set error if file was not uploaded
    if (!$file) {
      form_set_error($fieldName, 'Error uploading file.');
      return;
    }
     
    // set files to form_state, to process when form is submitted
    $form_state['values']['file'] = $file;
  }
}

function body_bg_action_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  $update = FALSE;
  if (isset($values['bid'])) {
    $update = TRUE;
    $result = db_query("SELECT * FROM {body_bg} WHERE bid = %d", $values['bid']);
    if ($result) {
      $bg = db_fetch_object($result);
    }
  }
  if (!$bg) {
    $bg = new StdClass();
  }
  $bg->name = $values['name'];
  $bg->bgcolor = $values['bgcolor'];
  $bg->paths = $values['paths'];
  $file = $values['file'];
  if ($file) {
    // make it permanent
    file_set_status($file, FILE_STATUS_PERMANENT);
    $bg->fid = $file->fid;
  }
  //save
  $result = drupal_write_record('body_bg', $bg, ($update ? 'bid' : NULL));
  if ($result == SAVED_NEW) {
    drupal_set_message("New custom background saved successfully!");
  }
  else if ($result == SAVED_UPDATED) {
    drupal_set_message("Exisiting custom background updated successfully!");
  }
  else {
    drupal_set_message("--error adding/updating custom background !<br/>");
  }
  // create CSS file
  create_css_file($bg);
  // clear css cache so that new content (as above) gets loaded in the
  // aggregated CSS (when CC optimixation is on)
  drupal_clear_css_cache();
}

function theme_body_bg_path_rules($form){
  //loop through each "row" in the table array
  foreach($form['rows'] as $id => $row){
    //we are only interested in numeric keys
    if (intval($id)){
      $this_row = $row['data']['#value'];

      //Add the weight field to the row
      $this_row[] = drupal_render($form['rows'][$id]['weight-'.$id]);

      //Add the row to the array of rows
      $table_rows[] = array('data' => $this_row, 'class'=>'draggable');
    }
  }

  //Make sure the header count matches the column count
  $header = array(
    "BG Name", "Image", "Edit", "Delete", "Order"
  );

  $output = theme('table', $header, $table_rows, array('id'=>'path-rules-table'));
  $output .= drupal_render($form);

  // Call add_tabledrag to add and setup the JS for us
  // The key thing here is the first param - the table ID
  // and the 4th param, the class of the form item which holds the weight
  drupal_add_tabledrag('path-rules-table', 'order', 'sibling', 'weight');

  return $output;
}

function body_bg_path_rules_submit($form, &$form_state) {
  $rows = $form_state['values']['bg_names']['rows'];
  foreach($rows as $row) {
    foreach($row as $key => $data){
      //we are only interested in weight elements
      if (substr($key,0,6) == 'weight'){
        //we have bid of the row in the element name
        $bid = str_replace('weight-','',$key);
        watchdog('body_bg', "UPDATE {body_bg} SET weight = ".$data." WHERE bid =".$bid, NULL, WATCHDOG_DEBUG);
        db_query("UPDATE {body_bg} SET weight = %d WHERE bid = %d", $data, $bid);
      }
    }
  }
}

function body_bg_theme() {
  return array(
    'body_bg_path_rules' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}