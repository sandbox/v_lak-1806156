               Custom Body Backgrounds
               -----------------------
DESCRIPTION
-----------

This module provides a means for specifying custom background image 
and background color for a page, as determined by the page's path.
One may specify wild-card characters (in the path) to set the same 
background for set of pages.

INSTALLATION
-----------
	Assuming you have cloned the GIT repository to a directory by name
'body_bg':

1) Copy body_bg directory (containing body_bg.info, body_bg.install and
bod_bg.module files) to your modules directory

2) Enable the module at: /admin/build/modules

3) Add custom background at: /admin/settings/body-backgrounds/add
3a) Upload an image file of your choice to be used as the background.
3b) In the 'Paths' textarea, enter a Drupal path per line. 
The '*' character is a wildcard. Example paths are blog for the blog 
page and blog/* for every personal blog. <front> is the front page
3c) Specify the background color to use (default: #000000) in html/CSS notation

4) To view a list of all custom bakgrounds, visit /admin/settings/body-backgrounds,
from where you may also edit existing ones.

5) Custom backgrounds as created above will add a html class called 'bg'
to the body class - a dynamic CSS file will be created like this:

body.bg {
 background-image: url("<uploaded image file>");
 background-color: <bg color>;
}

6) For the above to take effect, ensure the html output has the class 'bg'
for the body tag. To easily achieve this, in your theme's template file,

a) add the following to '<theme>_preprocess_page(&$vars, $hook)' method:
   $vars['body_classes'] .= ' bg';
or

b) add the following to 'phptemplate_body_class($left, $right)' method:
   $class .= ' bg';

as appropriate.   
